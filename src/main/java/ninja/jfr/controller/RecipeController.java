package ninja.jfr.controller;

import net.bytebuddy.implementation.bind.MethodDelegationBinder;
import ninja.jfr.model.Media;
import ninja.jfr.model.Recipe;
import ninja.jfr.service.MediaService;
import ninja.jfr.service.RecipeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class RecipeController {
    /**
     * logger
     */
    private Logger logger = LoggerFactory.getLogger(getClass());


    @Autowired
    private RecipeService recipeService;

    @Autowired
    private MediaService mediaService;

    @GetMapping("/showRecipes")
    public String showJobs(Model model) {
        ArrayList<Recipe> recipeList;
        recipeList = recipeService.getAllRecipes();
        model.addAttribute("recipeList", recipeList);
        return "showRecipes";
    }

    @GetMapping("/img/{id}")
    @ResponseBody
    public ResponseEntity<InputStreamResource> getMediaFile(@PathVariable("id") int id)
            throws IOException {
        Media media = mediaService.getMedia(id);
        if(media == null) {
            logger.warn("Couldn't find media with the id \"{}\"", id);
            return ResponseEntity.notFound().build();
        }
        File file = mediaService.getFile(media);
        MediaType mediaType = MediaType.valueOf(media.getMimeType());
        return ResponseEntity.ok().contentLength(file.length()).contentType(mediaType)
                .body(new InputStreamResource(mediaService.readFile(media)));
    }

    @GetMapping("/addRecipe")
    public String getAddRecipe(Model model) {   model.addAttribute("recipe", new Recipe());
        return "addRecipe";
    }

    @PostMapping("/addRecipe")
    public String postAddRecipe(@RequestParam("medias")List<MultipartFile> files ,@RequestParam("title")String title,@RequestParam("description")String description,@RequestParam("shoppingList")String shoppingList,@RequestParam("instructions")String instructions) throws IOException {
        List<Media> medias = new ArrayList<>(files.size());
        for(MultipartFile file : files) {
            File mediaFile = mediaService.saveMultipartFile(file);
            Media media = new Media(file.getOriginalFilename(), mediaFile.getName());

            if (media.getMimeType().startsWith("image/")){
                media.setTitle(media.getTitle().substring(0, media.getTitle().lastIndexOf('.')) + ".jpg");
                media.setMimeType("image/jpg");
            }
            Media savedMedia = mediaService.save(media);
            medias.add(savedMedia);
        }
        createRecipe(medias, title, description, instructions, shoppingList);
        return "redirect:/showRecipes";
    }

    public void createRecipe(List<Media> medias, String title, String description, String instructions, String shoppinglist) {
        Recipe recipe = new Recipe(title, description, shoppinglist, instructions, medias);
         recipe = recipeService.save(recipe);
         for(Media m : medias) {
             m.setRecipe(recipe);
             mediaService.save(m);
         }
    }

    @GetMapping("/allMedias")
    public String getAllMedias(Model model){
        ArrayList<Media> listOfMedia;
        listOfMedia = mediaService.getAllMedias();
        System.out.println(listOfMedia.size());
        model.addAttribute("mediaList", listOfMedia);
        return "allMedias";
    }

    public Media ConvertImg(){

        return null;
    }

}
