package ninja.jfr.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Recipe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String title;

    private String description;

    private String shoppingList;

    private String instructions;

    @OneToMany(mappedBy = "recipe", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private List<Media> medias = new ArrayList<>();

    public Recipe() {
    }

    public Recipe(String title, String description, String shoppingList, String instructions,
                  List<Media> medias) {
        super();
        this.title = title;
        this.description = description;
        this.shoppingList = shoppingList;
        this.instructions = instructions;
        this.medias = medias;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShoppingList() {
        return shoppingList;
    }

    public void setShoppingList(String shoppingList) {
        this.shoppingList = shoppingList;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public List<Media> getMedias() {
        return medias;
    }

    public void setMedias(List<Media> medias) {
        this.medias = medias;
    }

    public String getFirstPicturePath() {
        for (int i = 0; i < this.medias.size(); i++) {
            Media media = this.medias.get(i);
            if (media.getMimeType().startsWith("image")) {
                return media.getFileName();
            }

        }
        return "/img/placeholder.jpg";
    }

}
