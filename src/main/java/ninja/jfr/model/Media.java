package ninja.jfr.model;

import java.io.File;

import javax.activation.MimetypesFileTypeMap;
import javax.persistence.*;

@Entity
@Table(name = "media")
public class Media {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String title;

	private String fileName;

	private String mimeType;

	@ManyToOne(fetch = FetchType.LAZY)
	private Recipe recipe;

	/**
	 * Empty constructor for hibernate
	 */
	public Media() {
	}

	public Media(String title, String fileName) {
		super();
		this.title = title;
		this.fileName = fileName;
		this.mimeType = createMimetype(title);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String path) {
		this.fileName = path;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}


	public Recipe getRecipe() {
		return recipe;
	}

	public void setRecipe(Recipe recipe) {
		this.recipe = recipe;
	}

	public static String createMimetype(String path) {
		File file = new File(path);
		return new MimetypesFileTypeMap().getContentType(file);
	}
}
