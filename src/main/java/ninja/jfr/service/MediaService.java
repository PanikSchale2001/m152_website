package ninja.jfr.service;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ninja.jfr.model.Media;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.MimetypesFileTypeMap;
import javax.imageio.ImageIO;

@Service
public class MediaService {
    private final static File MEDIA_FOLDER = new File("./medias");
    private final static int BUFFER_SIZE = 512;

    @Autowired
    private MediaRepository mediaRepository;

    public MediaService() {
    }

    public Media save(Media media) {
        System.out.println(media.getMimeType());
        System.out.println(media.getFileName());
        System.out.println(media.getTitle());
        System.out.println(media.getId());
        return mediaRepository.save(media);
    }

    public Media getMedia(int id) {
        return mediaRepository.findById(id).orElse(null);
    }

    public void delete(int id) {
        mediaRepository.deleteById(id);
    }

    public ArrayList<Media> getAllMedias() {
        return (ArrayList<Media>) mediaRepository.findAll();
    }

    public void deleteAll() {
        mediaRepository.deleteAll();
    }

    public File saveMultipartFile(MultipartFile file) throws IOException {


        if (!MEDIA_FOLDER.exists()) {
            MEDIA_FOLDER.mkdir();
        }
        File newFile = new File(file.getOriginalFilename());
        File mediaFile = Files.createTempFile(MEDIA_FOLDER.toPath(), "media-", ".media").toFile();
        InputStream in = file.getInputStream();
        if (getMemeType(file.getOriginalFilename()).startsWith("image/")) {
            newFile = new File(newFile.getParent(), mediaFile.getName().substring(0, mediaFile.getName().lastIndexOf('.')) + ".jpg");


            try (OutputStream out = new FileOutputStream(mediaFile)) {
                BufferedImage image = ImageIO.read(file.getInputStream());
                Graphics graphics = image.getGraphics();

                graphics.setFont(new Font("Arial", Font.BOLD,50));
                graphics.drawString("Postet on rezepte.jfr.ninja",5,(image.getHeight() - 20));


               ImageIO.write(image, "jpg", out);
            }
        } else {

            try (OutputStream out = new FileOutputStream(mediaFile)) {
                int bytesRead = -1;
                byte[] buffer = new byte[BUFFER_SIZE];
                do {
                    bytesRead = in.read(buffer);
                    if (bytesRead < 0) break;

                    out.write(buffer, 0, bytesRead);
                } while (bytesRead > 0);
            }
        }
        return mediaFile;
    }

    public String getMemeType(String path) {
        File file = new File(path);
        return new MimetypesFileTypeMap().getContentType(file);
    

    }

    public InputStream readFile(Media m) throws FileNotFoundException {
        File mediaFile = getFile(m);
        return new FileInputStream(mediaFile);
    }

    public File getFile(Media m) {
        return new File(MEDIA_FOLDER, m.getFileName());
    }
}
