package ninja.jfr.service;

import org.springframework.data.repository.CrudRepository;

import ninja.jfr.model.Media;

public interface MediaRepository extends CrudRepository<Media, Integer>{

}
