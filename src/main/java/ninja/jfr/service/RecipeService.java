package ninja.jfr.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ninja.jfr.model.Recipe;

@Service
public class RecipeService{

	@Autowired
	private RecipeRepository recipeRepository;
	
	public RecipeService() {
	}
	
	public Recipe save(Recipe recipe) {
		return recipeRepository.save(recipe);
	}
	
	public Recipe getRecipe(int id) {
		return recipeRepository.findById(id).orElse(null);
	}
	
	public void delete(int id) {
		recipeRepository.deleteById(id);
	}
	
	public ArrayList<Recipe> getAllRecipes(){
		return (ArrayList<Recipe>) recipeRepository.findAll();
	}
	
	public void deleteAll() {
		recipeRepository.deleteAll();
	}
	
}
