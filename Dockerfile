FROM openjdk:11
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} /opt/m152_website/app.jar
COPY src/main/resources/application.properties /opt/m152_website/application.properties 
ENTRYPOINT ["java", "-jar", "/opt/m152_website/app.jar", "--spring.config.location=/opt/m152_website/application.properties"]
